created: 20210823205645582
modified: 20210825210836273
tags: 
title: Character Creator

\define advantageRollActions(field)
	<$action-randval $field=$field$ $upper=6 $numrolls=3/>
\end

\define levelHPRollActions()
  <$action-randval $field=hp_lvl_$(theLevel)$ $upper=$(theHitDie)$/>
  <$set name=nextLevel filter='[[$(theLevel)$]add[1]addprefix[hp_lvl_]]'><$list filter='[<currentTiddler>!has<nextLevel>]' variable=unused>
    <$action-setfield nextLevel=0/>
  </$list></$set>
\end

\define distributedStatIncDec()
<div style='display:inline-block'>
<$wikify name=remainingPoints text="<<remainingDistributedPoints>>"><$list filter='[<raceDataTiddler>getindex<stat>add{!!$(stat)$}compare:number:lt[20]] [<remainingPoints>compare:number:gt[0]] +[nth[2]]' emptyMessage='<$button disabled=yes>+</$button>' variable=unused><$button actions=<<addStatPoint $(stat)$>>>+</$button></$list></$wikify><br>
<$list filter='[<currentTiddler>get[$(stat)$_distributed]compare:number:gt[0]]' emptyMessage='<$button disabled=yes>-</$button>' variable=unused><$button actions=<<removeStatPoint stat>>>-</$button></$list>
</div>
\end

\define addStatPoint() <$list filter='[{!!$(stat)$_distributed}add[1]else[1]]' variable=newVal><$action-setfield $field=$(stat)$_distributed $value=<<newVal>>/></$filter>

\define removeStatPoint() <$list filter='[{!!$(stat)$_distributed}subtract[1]]' variable=newVal><$action-setfield $field=$(stat)$_distributed $value=<<newVal>>/></$filter>

\define usedDistributedPoints() <$wikify text="<$list filter='[<currentTiddler>fields[]suffix[_distributed]]' variable=theValues>{{{ [<currentTiddler>get<theValues>sum[]addsuffix[ ]] }}}</$list>" name=valueList><$text text={{{ [<valueList>enlist-input:raw[]sum[]] }}}/></$wikify>

\define remainingDistributedPoints() <$wikify name=totalPoints text="<<totalDistributedPoints>>"><$wikify name=usedPoints text="<<usedDistributedPoints>>"><$text text={{{ [<totalPoints>subtract<usedPoints>] +[else[0]] }}}/></$wikify></$wikify>

\define totalDistributedPoints() <$wikify text="<$list filter='[range<charLevel>addprefix[distribute_lvl_]]' variable=levelValue><$list filter='[<characterClass>addsuffix[ - Data]getindex<levelValue>addsuffix[ ]]'><<currentTiddler>></$list></$list>" name=valuelist><$text text={{{ [<valuelist>enlist-input:raw[]] =[{!!species}addsuffix[ - Data]getindex[distributable_points]else[0]] +[sum[]] }}}/></$wikify>

<$tiddler tiddler='$:/info/characterdemo'>
<$vars characterClass={{!!class}}>
<$wikify name=charLevel text="<<getCharacterLevel>>">
<$wikify name=theHitDie text="<<getClassInfo hitdie>>">
<$set name=raceDataTiddler filter='[{!!species}addsuffix[ - Data]]' select=0>

<table>
  <tr>
    <th>
      Name
    </th>
    <td>
      <$edit-text field='name'/>
    </td>
    <th>
      Player
    </th>
    <td>
      <$edit-text field='player'/>
    </td>
  </tr>
  <tr>
    <th>
      Race
    </th>
    <td>
      <$select class='attrselect' field=species><optgroup label="Player Species"><$list filter='[tag[Player Race]removesuffix[ - Data]]'><option><$view field='title'/></option></$list></optgroup><optgroup label="Monsters"><$list filter='[[Data - Lists]getindex[monster_species]enlist-input[]]'><option><$view field='title'/></option></$list></optgroup></$select>
    </td>
    <th>
      Experience
    </th>
    <td>
      <$edit-text field='xp'/>
    </td>
  </tr>
  <tr>
    <th>
      Class
    </th>
    <td>
      <$select class='attrselect' field=class><$list filter='[tag[Character Class]removesuffix[ - Data]]'><option><$view field='title'/></option></$list></$select>
    </td>
    <th>
      Current HP
    </th>
    <td>
      <$edit-text field='current_hp'/>
    </td>
  </tr>
  <tr>
    <th>
      Size
    </th>
    <td>
      <$list 
        filter='[<raceDataTiddler>getindex[size]]'
        emptyMessage="<$select class='attrselect' field=size><$list filter='[[Data - Lists]getindex[sizes]enlist-input[]]'><option><$view field='title'/></option></$list></$select>"
      >
        <$view field='title'/>
      </$list>
    </td>
    <th>
      Temporary HP
    </th>
    <td>
      <$edit-text field='temporary_hp'/>
    </td>
  </tr>
  <tr>
    <th>
      Type
    </th>
    <td>
      <$list 
        filter='[<raceDataTiddler>getindex[creature_type]]'
        emptyMessage="<$select class='attrselect' field=creature_type><$list filter='[[Data - Lists]getindex[creature_types]enlist-input[]]'><option><$view field='title'/></option></$list></$select>"
      >
        <$view field='title'/>
      </$list>
    </td>
    <th>
      HP Type
    </th>
    <td>
      <$radio tiddler='Settings' field='hp_type' value=roll>Rolled HP</$radio>
      <$radio tiddler='Settings' field='hp_type' value=mean>Average HP</$radio>
    </td>
  </tr>
  <tr>
    <th>
      Alignment
    </th>
    <td>
      <$select class='attrselect' field=alignment><$list filter='[[Data - Lists]getindex[alignments]enlist-input[]]'><option><$view field='title'/></option></$list></$select>
    </td>
  </tr>
</table>

<table>
<tr><th>Stat</th><th>Total</th><th>Die Roll</th><th>Racial Bonus</th><th>Distributable<br><<usedDistributedPoints>> of <<totalDistributedPoints>> used</th><th></th></tr>
<tr><th>STR</th><th><$text text={{{ =[<raceDataTiddler>getindex[str]] =[{!!str}add{!!str_distributed}] +[sum[]] }}}/></th><td>{{!!str}}</td><td><$text text={{{ [<raceDataTiddler>getindex[str]] }}}/></td><td> <$text text={{{ [<currentTiddler>has[str_distributed]get[str_distributed]else[0]] }}}/> <$vars stat=str><<distributedStatIncDec>></$vars> </td><td><$button actions="<<advantageRollActions str>>">Roll</$button></td></tr>
<tr><th>DEX</th><th><$text text={{{ =[<raceDataTiddler>getindex[dex]] =[{!!dex}add{!!dex_distributed}] +[sum[]] }}}/></th><td>{{!!dex}}</td><td><$text text={{{ [<raceDataTiddler>getindex[dex]] }}}/></td><td> <$text text={{{ [<currentTiddler>has[dex_distributed]get[dex_distributed]else[0]] }}}/> <$vars stat=dex><<distributedStatIncDec>></$vars> </td><td><$button actions="<<advantageRollActions dex>>">Roll</$button></td></tr>
<tr><th>CON</th><th><$text text={{{ =[<raceDataTiddler>getindex[con]] =[{!!con}add{!!con_distributed}] +[sum[]] }}}/></th><td>{{!!con}}</td><td><$text text={{{ [<raceDataTiddler>getindex[con]] }}}/></td><td> <$text text={{{ [<currentTiddler>has[con_distributed]get[con_distributed]else[0]] }}}/> <$vars stat=con><<distributedStatIncDec>></$vars> </td><td><$button actions="<<advantageRollActions con>>">Roll</$button></td></tr>
<tr><th>INT</th><th><$text text={{{ =[<raceDataTiddler>getindex[int]] =[{!!int}add{!!int_distributed}] +[sum[]] }}}/></th><td>{{!!int}}</td><td><$text text={{{ [<raceDataTiddler>getindex[int]] }}}/></td><td> <$text text={{{ [<currentTiddler>has[int_distributed]get[int_distributed]else[0]] }}}/> <$vars stat=int><<distributedStatIncDec>></$vars> </td><td><$button actions="<<advantageRollActions int>>">Roll</$button></td></tr>
<tr><th>WIS</th><th><$text text={{{ =[<raceDataTiddler>getindex[wis]] =[{!!wis}add{!!wis_distributed}] +[sum[]] }}}/></th><td>{{!!wis}}</td><td><$text text={{{ [<raceDataTiddler>getindex[wis]] }}}/></td><td> <$text text={{{ [<currentTiddler>has[wis_distributed]get[wis_distributed]else[0]] }}}/> <$vars stat=wis><<distributedStatIncDec>></$vars> </td><td><$button actions="<<advantageRollActions wis>>">Roll</$button></td></tr>
<tr><th>CHA</th><th><$text text={{{ =[<raceDataTiddler>getindex[cha]] =[{!!cha}add{!!cha_distributed}] +[sum[]] }}}/></th><td>{{!!cha}}</td><td><$text text={{{ [<raceDataTiddler>getindex[cha]] }}}/></td><td> <$text text={{{ [<currentTiddler>has[cha_distributed]get[cha_distributed]else[0]] }}}/> <$vars stat=cha><<distributedStatIncDec>></$vars> </td><td><$button actions="<<advantageRollActions cha>>">Roll</$button></td></tr>
</table>

<div style='position:relative;vertical-align:top;'>
<$list filter='[[Settings]get[hp_type]match[roll]]' variable=unused>
<table style='display:inline-block'>
  <tr>
    <th>
      Level
    </th>
    <th>
      Value
    </th>
    <th>
      Reroll
    </th>
  </tr>
  <tr>
    <th>
      Level 1
    </th>
    <td>
      <$text text={{{ [{!!class}addsuffix[ - Data]getindex[hitdie]] }}}/>
    </td>
    <td>
    </td>
  </tr>
  <$list filter='[range<charLevel>bf[]limit[9]]' variable=theLevel>
    <tr>
      <th>
        Level <$text text=<<theLevel>>/>
      </th>
      <td>
        <$set name=theField filter="[<theLevel>addprefix[hp_lvl_]]" select=0><$view field=<<theField>>/></$set>
      </td>
      <td>
        <$button actions=<<levelHPRollActions>>>Roll</$button>
      </td>
    </tr>
  </$list>
</table>

<$list filter='[<charLevel>compare:number:gt[10]]' variable=unused>
<table style='position:absolute;display:inline-block;top:0px;'>
  <tr>
    <th>
      Level
    </th>
    <th>
      Value
    </th>
    <th>
      Reroll
    </th>
  </tr>
  <$list filter='[range<charLevel>bf[10]limit[10]]' variable=theLevel>
    <tr>
      <th>
        Level <$text text=<<theLevel>>/>
      </th>
      <td>
        <$set name=theField filter="[<theLevel>addprefix[hp_lvl_]]" select=0><$view field=<<theField>>/></$set>
      </td>
      <td>
        <$button
          actions=<<levelHPRollActions>>
        >
          Roll
          <$set 
            name=nextLevel 
            filter='[<theLevel>add[1]addprefix[hp_lvl_]]'
          >
            <$list
              filter='[<currentTiddler>!has<nextLevel>]' 
              variable=unused
            >
              <$action-setfield
                $field=<<nextLevel>> 
                $value=0
              />
            </$list>
          </$set>
        </$button>
      </td>
    </tr>
  </$list>
</table>
</$list>
</$list>

<$list filter='[[Settings]get[hp_type]!match[roll]]' variable=unused>
<table style='display:inline-block'>
  <tr>
    <th>
      Level
    </th>
    <th>
      Value
    </th>
  </tr>
  <tr>
    <th>
      Level 1
    </th>
    <td>
      <$text text={{{ [{!!class}addsuffix[ - Data]getindex[hitdie]] }}}/>
    </td>
  </tr>
  <$list filter='[range<charLevel>bf[]limit[9]]' variable=theLevel>
    <tr>
      <th>
        Level <$text text=<<theLevel>>/>
      </th>
      <td>
        <$wikify name=meanVal text="<<getClassInfo meanhitdie>>"><<meanVal>></$wikify>
      </td>
    </tr>
  </$list>
</table>

<$list filter='[<charLevel>compare:number:gt[10]]' variable=unused>
<table style='position:absolute;display:inline-block;top:0px;'>
  <tr>
    <th>
      Level
    </th>
    <th>
      Value
    </th>
  </tr>
  <$list filter='[range<charLevel>bf[10]limit[10]]' variable=theLevel>
    <tr>
      <th>
        Level <$text text=<<theLevel>>/>
      </th>
      <td>
        <$wikify name=meanVal text="<<getClassInfo meanhitdie>>"><<meanVal>></$wikify>
      </td>
    </tr>
  </$list>
</table>
</$list>
</$list>
</div>

</$set>
</$wikify>
</$wikify>

<$checkbox field='inspiration' checked=✘ unchecked=''>Inspiration</$checkbox>

</$vars>
{{||Template/Character}}
</$tiddler>