created: 20210822195339002
modified: 20210825195408378
tags: 
title: Template/Character

<style>
.flex-container {
	display:flex;
}
.statbox {
	flex: 1;
	text-align: center;
	border: solid 2px;
	margin: 10px;
	border-radius: 20px;
	font-weight: bold;
}
.inner-statbox {
	border-radius: 35%;
	border: solid 1px;
	width: 30%;
	margin-left: 35%;
	margin-right: 35%;
	font-weight:bold;
}
.character-name {
	text-align: center;
	font-weight: bold;
	font-size: 50px;
	margin: 0px;
}
.ac-badge {
  flex: 1;
  border: solid 1px;
  border-radius: 3px;
  text-align: center;
}
</style>

<$vars characterClass={{!!class}} characterRace={{!!species}}>
<$wikify name=charLevel text="<<getCharacterLevel>>">
<$wikify name=theHitDie text="<<getClassInfo hitdie>>">
<$set name=raceDataTiddler filter='[{!!species}addsuffix[ - Data]]' select=0>

<<temp>>

<div style='padding: 30px;border-radius:60px;;background-color:RGB(250,247,235);'>

<h1 class='character-name'>{{!!name}}</h1>
<div style='text-align:center;margin:0px;padding:0px;'>Player: {{!!player}}</div>
<div style='display:flex;'>
<div style='flex:1;text-align:center;'>Class:<br>{{!!class}}</div><div style='flex:1;text-align:center;'>Level:<br><<getCharacterLevel>></div><div style='flex:1;text-align:center;'>Alignment:<br>{{!!alignment}}</div><div style='flex:1;text-align:center;'>Race:<br>{{!!species}}</div><div style='flex:1;text-align:center;'>Background:<br>{{!!background}}</div><div style='flex:1;text-align:center;'>Experience:<br>{{!!xp}}</div>
</div>

<div
	class='flex-container'
	style='display:flex'
>
	<div
		class=statbox
	>
		Strength<br>
		<$text text={{{ =[<raceDataTiddler>getindex[str]] =[{!!str}add{!!str_distributed}] +[sum[]] }}}/>
		<div class='inner-statbox'>
			<<statBonus str>>
		</div>
	</div>
	<div 
    class=statbox
  >
    Dexterity<br>
    <$text text={{{ =[<raceDataTiddler>getindex[dex]] =[{!!dex}add{!!dex_distributed}] +[sum[]] }}}/>
    <div class='inner-statbox'>
			<<statBonus dex>>
		</div>
  </div>
	<div 
    class=statbox
  >
    Constitution<br>
    <$text text={{{ =[<raceDataTiddler>getindex[con]] =[{!!con}add{!!con_distributed}] +[sum[]] }}}/>
    <div class='inner-statbox'>
			<<statBonus con>>
		</div>
  </div>
	<div 
    class=statbox
  >
    Intelligence<br>
    <$text text={{{ =[<raceDataTiddler>getindex[int]] =[{!!int}add{!!int_distributed}] +[sum[]] }}}/>
    <div class='inner-statbox'>
			<<statBonus int>>
		</div>
  </div>
	<div 
    class=statbox
  >
    Wisdom<br>
    <$text text={{{ =[<raceDataTiddler>getindex[wis]] =[{!!wis}add{!!wis_distributed}] +[sum[]] }}}/>
    <div class='inner-statbox'>
			<<statBonus wis>>
		</div>
  </div>
	<div 
    class=statbox
  >
    Charisma<br>
    <$text text={{{ =[<raceDataTiddler>getindex[cha]] =[{!!cha}add{!!cha_distributed}] +[sum[]] }}}/>
    <div class='inner-statbox'>
			<<statBonus cha>>
		</div>
  </div>
</div>

<div style='display:flex;flex-direction:row;justify-content:space-evenly'>
  <div style='flex:1;border: solid 1px;border-radius:40px;max-width:200px;display:inline-block;'>
    <div style='font-weight:bold;text-align:center;border-bottom:solid 2px;line-height:40px;'>Saving Throws</div>
    <div style='padding-left:20px;'>
      <table style='border:none;'>
      <tr><td style='border:none;font-weight:bold;'>Strength</td><td style='border:none;text-align:right;'><<charStatSaves str>></td></tr>
      <tr><td style='border:none;font-weight:bold;'>Dexterity</td><td style='border:none;text-align:right;'><<charStatSaves dex>></td></tr>
      <tr><td style='border:none;font-weight:bold;'>Constitution</td><td style='border:none;text-align:right;'><<charStatSaves con>></td></tr>
      <tr><td style='border:none;font-weight:bold;'>Intelligence</td><td style='border:none;text-align:right;'><<charStatSaves int>></td></tr>
      <tr><td style='border:none;font-weight:bold;'>Wisdom</td><td style='border:none;text-align:right;'><<charStatSaves wis>></td></tr>
      <tr><td style='border:none;font-weight:bold;'>Charisma</td><td style='border:none;text-align:right;'><<charStatSaves cha>></td></tr>
      </table>
    </div>
  </div>
  <div
    style='flex:1;display:flex;flex-direction:column;'
  >
    <div style='display:flex;'>
      <div
        class='ac-badge'
      >
          AC:<br><<getCalculatedAC>>
      </div>
      <div
        class='ac-badge'
      >
          Initiative:<br><<getInitiative>>
      </div>
      <div
        class='ac-badge'
      >
          Speed:<br><$wikify text="<<getRaceInfo speed>>" name=thing><<thing>></$wikify>
      </div>
    </div>
    <div
      style='display:flex;'
    >
      <div
        class='ac-badge'
      >
        Passive Wisdom (perception)<br><<getPassivePerception>>
      </div>
      <div
        class='ac-badge'
      >
        Inspiration<br>{{!!inspiration}}
      </div>
      <div
        class='ac-badge'
      >
        Proficiency Bonus<br>+<<getProficiencyBonus>>
      </div>
    </div>
    <div
      style='display:flex;'
    >
      <div
        class='ac-badge'
      >
        Temporary HP<br>{{!!temporary_hp}}
      </div>
      <div
        class='ac-badge'
      >
        Current HP<br>{{!!current_hp}}
      </div>
      <div
        class='ac-badge'
      >
        Maximum HP<br><<getMaximumHP>>
      </div>
    </div>
  </div>
</div>

</div>
</$set>
</$wikify>
</$wikify>
</$vars>